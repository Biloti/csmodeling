<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE flow PUBLIC "-//GEBR//DTD FLOW 0.4.0//EN" "http://gebr.googlecode.com/hg/libgebr/geoxml/data/flow-0.4.0.dtd">
<flow version="0.4.0">
  <title>CSmodeling</title>
  <description>CShot modeling wrapper</description>
  <help>&lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"&gt;&lt;html xmlns="http://www.w3.org/1999/xhtml"&gt;&lt;head&gt;  &lt;meta http-equiv="content-type" content=  "text/html; charset=utf-8" /&gt;  &lt;link rel="stylesheet" type="text/css" href="gebr.css" /&gt;  &lt;title&gt;    &lt;!-- begin ttl --&gt;CSmodeling&lt;!-- end ttl --&gt;  &lt;/title&gt;&lt;/head&gt;&lt;body&gt;  &lt;div class="outer-container"&gt;    &lt;div class="inner-container"&gt;      &lt;div class="header"&gt;        &lt;div class="title"&gt;          &lt;span class="flowtitle"&gt;            &lt;!-- begin tt2 --&gt;CSmodeling&lt;!-- end tt2 --&gt;          &lt;/span&gt;          &lt;div class="description"&gt;            &lt;!-- begin des --&gt;CShot modeling wrapper&lt;!-- end des --&gt;          &lt;/div&gt;        &lt;/div&gt;      &lt;/div&gt;      &lt;div class="category"&gt;        &lt;!-- begin cat --&gt;Simulation and Model Building | INCT - GP | GGC - UNICAMP&lt;!-- end cat --&gt;      &lt;/div&gt;      &lt;div class="main"&gt;        &lt;div class="content"&gt;          &lt;!-- begin cnt --&gt;&lt;h2 id="intro"&gt;	Introduction&lt;/h2&gt;&lt;p&gt;	Csmodeling is a modeling tool to generate 2D synthetic datasets by ray tracing. It is a wrapper for the modeling tool CShot, distributed as part of the Seismic Un*x package.&lt;/p&gt;&lt;h2 id="credits"&gt;	Credits&lt;/h2&gt;&lt;p&gt;	Nov 18, 2016: written by Ricardo Biloti &amp;lt;biloti@gebrproject.com&amp;gt;&lt;/p&gt;&lt;!-- end cpy --&gt;&lt;!-- end cnt --&gt;        &lt;/div&gt;        &lt;div class="navigation"&gt;          &lt;h2&gt;Index&lt;/h2&gt;          &lt;ul&gt;            &lt;li&gt;&lt;a href="#intro"&gt;Introduction&lt;/a&gt;&lt;/li&gt;	    &lt;!-- begin mpr --&gt;            &lt;li&gt;&lt;a href="#par"&gt;Parameters&lt;/a&gt;&lt;/li&gt;	    &lt;!-- end mpr --&gt;            &lt;li&gt;&lt;a href="#details"&gt;Description&lt;/a&gt;&lt;/li&gt;            &lt;li&gt;&lt;a href="#notes"&gt;Notes&lt;/a&gt;&lt;/li&gt;            &lt;li&gt;&lt;a href="#ref"&gt;References&lt;/a&gt;&lt;/li&gt;            &lt;li&gt;&lt;a href="#credits"&gt;Credits&lt;/a&gt;&lt;/li&gt;          &lt;/ul&gt;        &lt;/div&gt;        &lt;div class="clearer"&gt;&lt;/div&gt;      &lt;/div&gt;      &lt;div class="footer"&gt;        &lt;span class="left"&gt;G&amp;ecirc;BR 0.21.1 (&lt;!-- begin dtd --&gt;0.4.0&lt;!-- end dtd --&gt;) |		&lt;!-- begin ver --&gt;Nov 18, 2016&lt;!-- end ver --&gt;&lt;/span&gt;        &lt;div class="clearer"&gt;&lt;/div&gt;      &lt;/div&gt;    &lt;/div&gt;  &lt;/div&gt;&lt;/body&gt;&lt;/html&gt;</help>
  <author>Ricardo Biloti</author>
  <email>biloti@gebrproject.com</email>
  <dict>
    <parameters default-selection="0"/>
  </dict>
  <parent/>
  <date>
    <created>2016-11-18T16:39:21.144590Z</created>
    <modified>2017-05-04T20:13:05.495605Z</modified>
    <lastrun/>
  </date>
  <category>Simulation and Model Building</category>
  <category>INCT - GP</category>
  <category>GGC - UNICAMP</category>
  <server group-type="group" group-name="">
    <io>
      <input/>
      <output/>
      <error/>
    </io>
    <lastrun/>
  </server>
  <program stdin="no" stdout="no" stderr="yes" status="configured" mpi="" version="1.0">
    <title>CSmodeling</title>
    <binary>csmodeling</binary>
    <description>CShot modeling wrapper</description>
    <help><![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content=
  "text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="gebr.css" />
  <title>
    <!-- begin ttl -->CSmodeling<!-- end ttl -->
  </title>
</head>

<body>
  <div class="outer-container">
    <div class="inner-container">
      <div class="header">
        <div class="title">
          <span class="flowtitle">
            <!-- begin tt2 -->CSmodeling<!-- end tt2 -->
          </span>
          <div class="description">
            <!-- begin des -->CShot modeling wrapper<!-- end des -->
          </div>
        </div>
      </div>

      <div class="category">
        <!-- begin cat -->Simulation and Model Building | INCT - GP | GGC - UNICAMP<!-- end cat -->
      </div>

      <div class="main">
        <div class="content">
          <!-- begin cnt --><h2 id="intro">
	Introduction</h2>
<p>
	Csmodeling is wrapper to ray-tracing modling tool CShot, distributed as part of Seismic Un*x. By means of csmodeling, a 2D acquisition line can be simulated. CShot supports acustic models, composed by a stack of homogeneous layers. Direct or primary reflected rays are generated automatically, while other rays paths can be manually provided.</p>
<div>
	Csmodeling generates high quality PDF/SVG figures depicting the model and/or rays, through Gnuplot. The Gnuplot source script and data is kept, so they can be further tuned by the user.</div>
<!-- begin par --><div class="parameters">
	<h2 id="par">
		Parameters</h2>
	<ul>
		<li class="group">
			<span class="grouplabel">Interfaces</span><br />
			detailed description comes here.
			<ul>
				<li>
					<span class="reqlabel">Knots for interface interpolation</span></li>
			</ul>
		</li>
		<li>
			<span class="reqlabel">Velocities below each interface</span></li>
		<li class="group">
			<span class="grouplabel">Stations</span><br />
			detailed description comes here.
			<ul>
				<li>
					<span class="label">Reference station index</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Reference station x coordinate</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Distance between consecutive stations</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Receiver&#39;s depth</span><br />
					detailed description comes here.</li>
			</ul>
		</li>
		<li class="group">
			<span class="grouplabel">Seismograms</span><br />
			detailed description comes here.
			<ul>
				<li>
					<span class="label">Maximum recorded time (secs)</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Sampling rate (microseconds)</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Wavelet length (secs)</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Lowest frequency of the source</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">First frequency with full amplitude</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Last frequency with full amplitude</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Highest frequency of the source</span><br />
					detailed description comes here.</li>
			</ul>
		</li>
		<li class="group">
			<span class="grouplabel">Wave types</span><br />
			detailed description comes here.
			<ul>
				<li>
					<span class="label">Direct wave</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Primaries</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Reflection at bottom interface</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Takeoff angle (degree)</span><br />
					detailed description comes here.</li>
				<li>
					<span class="label">Takeoff angle increment</span><br />
					detailed description comes here.</li>
			</ul>
		</li>
		<li class="group">
			<span class="grouplabel">Extra ray codes</span><br />
			Specifies as many ray codes as needed.
			<ul>
				<li>
					<span class="label">Ray code (sequence of interfaces where ray reflects)</span><br />
					This list is space separated. For instance, 1 0 1 specifies the surface multiple
					for the first reflector.</li>
			</ul>
		</li>
		<li>
			<span class="label">Well coordinates</span><br />
			They should be specified as x1,z1;x2,z2;...;xn,zn.</li>
		<li>
			<span class="label">Number of regular shots</span><br />
			When specified, the first shot code (see parameter below) will be shifted in a regular fashion to
			describe futher shots.</li>
		<li>
			<span class="label">Displacement between regular shots (in stations)</span><br />
			Integer number of stations to shift the shot geometry in a regular acquisition.</li>
			
		<li class="group">
			<span class="grouplabel">Shots</span><br />
			The next parameter can be provided as many times as needed, to specify
			several shot configurations.
			<ul>
				<li>
					<span class="label">Shot code</span><br />
					Each shot code is composed by four integer number and two real numbers,
					separated by commas: i0,i1,i2,i3,is,zs.<br />
					Receiver are allocated from station i0 to i1, and from i2 to i3. This
					allows to have gap inside the receivers' location range. zs is the location of
					source in terms of station's index. zs is a real number, meaning that the location
					can be between to stations. zs is the depth of the sources. To clarify this parameter,
					consider the example:<br/><br/>
					&nbsp;&nbsp;40,100,120,180,110.0,0.010<br/><br/>
					This means that the shot is exactly over the station number 110, burried at 10&nbps;m.
					The receivers are allocated from station 40 up to 100, and from station 120 up to 180,
					which means that no trace will be recorded at stations 101,102,...,119.</li>
			</ul>
		</li>
		<li class="group">
			<span class="grouplabel">Export</span><br />
			Parameters to control the SVG/PDF output of the model and rays</li>
			<ul>
				<li>
					<span class="label">Suppress export to gnuplot</span><br />
					When checked, it turns off the export of model/rays completely.</li>
				<li>
					<span class="label">Suppress interfaces</span><br />
					When checked, the interfaces are not displayed.</li>
				<li>
					<span class="label">Plot rays</span><br />
					When checked, some rays are depicted over the model.</li>
				<li>
					<span class="label">Plot one ray at every</span><br />
					As many rays may have been computed, ploting every ray may lead to fill the
					figure. This parameter has the effect of decimate the rays plotted.
					For instance, set it to 5 means that from each 5 consecutive rays, only
					one will be ploted.</li>
				<li>
					<span class="label">Maximum number of rays to plot</span><br />
					Maximum number of rays that will be ploted. The rays are collected in the
					order they were computed during the modeling.</li>
				<li>
					<span class="label">Minimum x coordinate in figure</span><br /></li>
				<li>
					<span class="label">Maximum x coordinate in figure</span><br /></li>
				<li>
					<span class="label">Minimum z coordinate in figure</span><br /></li>
				<li>
					<span class="label">Maximum z coordinate in figure</span><br /></li>
				<li>
					<span class="label">X coordinate to show velocity's label</span><br />
					When specified, the velocity of each layer will be stamped over
					the layer, at that horizontal position. Leave it empty to
					supress these labels.</li>
				<li>
					<span class="label">Palette</span><br />
					Choose among some predefined palettes.</li>
			</ul>
		</li>
		<li>
			<span class="label">(spacial unit / meter) factor (1000 for kilometers)</span><br />
			detailed description comes here.</li>
		<li>
			<span class="reqlabel">Working directory</span></li>
		<li>
			<span class="reqlabel">Output directory name (inside working dir)</span></li>
		<li>
			<span class="label">Quiet execution</span><br />
			detailed description comes here.</li>
	</ul>
<!-- end lst --></div>
<!-- end par --><h2 id="details">
	Credits</h2>
<p>
	This program is developed by Ricardo Biloti, Department of Applied Mathematics, University of Campinas.</p>
<!-- begin cpy --><!-- end cpy --><!-- end cnt -->
        </div>

        <div class="navigation">
          <h2>Index</h2>

          <ul>
            <li><a href="#intro">Introduction</a></li>

	    <!-- begin mpr -->
            <li><a href="#par">Parameters</a></li>
	    <!-- end mpr -->

            <li><a href="#details">Description</a></li>

            <li><a href="#notes">Notes</a></li>

            <li><a href="#ref">References</a></li>

            <li><a href="#credits">Credits</a></li>
          </ul>
        </div>

        <div class="clearer"></div>
      </div>

      <div class="footer">
        <span class="left">G&ecirc;BR 0.21.1 (<!-- begin dtd -->0.4.0<!-- end dtd -->) |
		<!-- begin ver -->1.0<!-- end ver --></span>

        <div class="clearer"></div>
      </div>
    </div>
  </div>
</body>
</html>
]]></help>
    <url>http://bitbucket.org/Biloti/csmodeling</url>
    <parameters default-selection="0">
      <parameter>
        <label>Interfaces</label>
        <group instanciable="yes" expand="yes">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Knots for interface interpolation</label>
                <string>
                  <property required="yes" separator=";">
                    <keyword>--knots=</keyword>
                    <value/>
                    <default/>
                  </property>
                </string>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0,0</default>
                  <default>10,0</default>
                </property>
              </reference>
            </parameter>
          </parameters>
          <parameters default-selection="0">
            <parameter>
              <label>Knots for interface interpolation</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0,1</default>
                  <default>5,1.2</default>
                  <default>10,1</default>
                </property>
              </reference>
            </parameter>
          </parameters>
          <parameters default-selection="0">
            <parameter>
              <label>Knots for interface interpolation</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0,2</default>
                  <default>10,2</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>Velocities below each interface</label>
        <float min="0">
          <property required="yes" separator=",">
            <keyword>--velocity=</keyword>
            <value/>
            <default>2</default>
            <default>3</default>
            <default>4</default>
          </property>
        </float>
      </parameter>
      <parameter>
        <label>Stations</label>
        <group instanciable="no" expand="no">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Reference station index</label>
                <int>
                  <property required="no">
                    <keyword>--istation=</keyword>
                    <value/>
                    <default>0</default>
                  </property>
                </int>
              </parameter>
              <parameter>
                <label>Reference station x coordinate</label>
                <float>
                  <property required="no">
                    <keyword>--xstation=</keyword>
                    <value/>
                    <default>0.0</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Distance between consecutive stations</label>
                <float>
                  <property required="no">
                    <keyword>--dstation=</keyword>
                    <value/>
                    <default>0.05</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Receiver's depth</label>
                <float>
                  <property required="no">
                    <keyword>--rdepth=</keyword>
                    <value/>
                    <default>0</default>
                  </property>
                </float>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0.0</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0.05</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0.0</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>Seismograms</label>
        <group instanciable="no" expand="no">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Maximum recorded time (secs)</label>
                <float min="">
                  <property required="no">
                    <keyword>--tmax=</keyword>
                    <value/>
                    <default>4.0</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Sampling rate (microseconds)</label>
                <int min="1">
                  <property required="no">
                    <keyword>--dt=</keyword>
                    <value/>
                    <default>4000</default>
                  </property>
                </int>
              </parameter>
              <parameter>
                <label>Wavelet length (secs)</label>
                <float>
                  <property required="no">
                    <keyword>--wlen=</keyword>
                    <value/>
                    <default>0.150</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Lowest frequency of the source</label>
                <float min="0">
                  <property required="no">
                    <keyword>--f0=</keyword>
                    <value/>
                    <default>10</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>First frequency with full amplitude</label>
                <float min="0">
                  <property required="no">
                    <keyword>--f1=</keyword>
                    <value/>
                    <default>25</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Last frequency with full amplitude</label>
                <float min="0">
                  <property required="no">
                    <keyword>--f2=</keyword>
                    <value/>
                    <default>35</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Highest frequency of the source</label>
                <float min="0">
                  <property required="no">
                    <keyword>--f3=</keyword>
                    <value/>
                    <default>50</default>
                  </property>
                </float>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>4.0</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>4000</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label>Sampling rate (microseconds)</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>0.150</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>10</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>25</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>35</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>50</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>Wave types</label>
        <group instanciable="no" expand="no">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Direct wave</label>
                <flag>
                  <property required="no">
                    <keyword>--direct</keyword>
                    <value>off</value>
                    <default>off</default>
                  </property>
                </flag>
              </parameter>
              <parameter>
                <label>Primaries</label>
                <flag>
                  <property required="no">
                    <keyword>--primary</keyword>
                    <value>off</value>
                    <default>on</default>
                  </property>
                </flag>
              </parameter>
              <parameter>
                <label>Reflection at bottom interface</label>
                <flag>
                  <property required="no">
                    <keyword>--bottom</keyword>
                    <value>off</value>
                    <default>off</default>
                  </property>
                </flag>
              </parameter>
              <parameter>
                <label>Takeoff angle (degree)</label>
                <float min="0" max="180">
                  <property required="no">
                    <keyword>--takeoff=</keyword>
                    <value/>
                    <default>80</default>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Takeoff angle increment</label>
                <float>
                  <property required="no">
                    <keyword>--inc=</keyword>
                    <value/>
                    <default>1</default>
                  </property>
                </float>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>off</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>on</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>off</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>80</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>1</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>Extra ray codes</label>
        <group instanciable="yes" expand="no">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Ray code (sequence of interfaces where ray reflects)</label>
                <string>
                  <property required="no" separator=" ">
                    <keyword>--raycode=</keyword>
                    <value/>
                    <default/>
                  </property>
                </string>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default/>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      
      <parameter>
        <label>Number of regular shots</label>
        <int min="">
          <property required="no">
            <keyword>--nshots=</keyword>
            <value/>
            <default>1</default>
          </property>
        </int>
      </parameter>
      <parameter>
        <label>Shot increment , in stations, for surface acquisition</label>
        <int>
          <property required="no">
            <keyword>--dxshot=</keyword>
            <value/>
            <default>2</default>
          </property>
        </int>
      </parameter>
      <parameter>
        <label>Well coordinates</label>
        <string>
          <property required="no" separator=";">
            <keyword>--well=</keyword>
            <value/>
            <default/>
          </property>
        </string>
      </parameter><parameter>
        <label>Distance between shots, for downhole acquisition</label>
        
      <float><property required="no">
            <keyword>--dwshot=</keyword>
            <value/>
            <default/>
          </property></float></parameter><parameter><label>Depth of the first source in cas of downhole acquisition</label><float><property required="no"><keyword>--sdepth=</keyword><value/><default/></property></float></parameter><parameter>
        <label>Shots</label>
        <group instanciable="yes" expand="yes">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Shot code</label>
                <string>
                  <property required="no" separator=",">
                    <keyword>--shot=</keyword>
                    <value/>
                    <default>60</default>
                    <default>90</default>
                    <default>110</default>
                    <default>140</default>
                    <default>100</default>
                    <default>0</default>
                  </property>
                </string>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>60</default>
                  <default>90</default>
                  <default>110</default>
                  <default>140</default>
                  <default>100</default>
                  <default>0</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>Export</label>
        <group instanciable="no" expand="no">
          <template-instance>
            <parameters default-selection="0">
              <parameter>
                <label>Suppress export to gnuplot</label>
                <flag>
                  <property required="no">
                    <keyword>--noglpt</keyword>
                    <value>off</value>
                    <default>off</default>
                  </property>
                </flag>
              </parameter>
              <parameter><label>Suppress well ploting</label><flag><property required="no"><keyword>--nowell</keyword><value>off</value><default>off</default></property></flag></parameter><parameter>
                <label>Suppress interfaces ploting</label>
                <flag>
                  <property required="no">
                    <keyword>--nointerfaces</keyword>
                    <value>off</value>
                    <default>off</default>
                  </property>
                </flag>
              </parameter>
              <parameter>
                <label>Plot rays</label>
                <flag>
                  <property required="no">
                    <keyword>--rays</keyword>
                    <value>off</value>
                    <default>off</default>
                  </property>
                </flag>
              </parameter>
              <parameter>
                <label>Plot one ray at every</label>
                <int min="1">
                  <property required="no">
                    <keyword>--decimate=</keyword>
                    <value/>
                    <default>1</default>
                  </property>
                </int>
              </parameter>
              <parameter>
                <label>Limit the maximum number of rays to plot</label>
                <int>
                  <property required="no">
                    <keyword>--limit=</keyword>
                    <value/>
                    <default>250</default>
                  </property>
                </int>
              </parameter>
              <parameter>
                <label>Minimum x coordinate in figure</label>
                <float>
                  <property required="no">
                    <keyword>--xmin=</keyword>
                    <value/>
                    <default/>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Maximum x coordinate in figure</label>
                <float>
                  <property required="no">
                    <keyword>--xmax=</keyword>
                    <value/>
                    <default/>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Minimum z coordinate in figure</label>
                <float>
                  <property required="no">
                    <keyword>--zmin=</keyword>
                    <value/>
                    <default/>
                  </property>
                </float>
              </parameter>
              <parameter>
                <label>Maximum z coordinate in figure</label>
                <float>
                  <property required="no">
                    <keyword>--zmax=</keyword>
                    <value/>
                    <default/>
                  </property>
                </float>
              </parameter>
              <parameter><label>X coordiante to show velocity's labels</label><float><property required="no"><keyword>--xvel=</keyword><value/><default/></property></float></parameter><parameter>
                <label>Palette</label>
                <range min="0" max="5" inc="1" digits="0">
                  <property required="no">
                    <keyword>--palette=</keyword>
                    <value/>
                    <default>2</default>
                  </property>
                </range>
              </parameter>
            </parameters>
          </template-instance>
          <parameters default-selection="0">
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>off</default>
                </property>
              </reference>
            </parameter>
            <parameter><label/><reference><property required="no"><keyword/><value/><default>off</default></property></reference></parameter><parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>off</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>on</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>1</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>250</default>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default/>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label>Minimum x coordinate in figure</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default/>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label>Minimum x coordinate in figure</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default/>
                </property>
              </reference>
            </parameter>
            <parameter>
              <label>Maximum x coordinate in figure</label>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default/>
                </property>
              </reference>
            </parameter>
            <parameter><label/><reference><property required="no"><keyword/><value/><default></default></property></reference></parameter><parameter>
              <label/>
              <reference>
                <property required="no">
                  <keyword/>
                  <value/>
                  <default>2</default>
                </property>
              </reference>
            </parameter>
          </parameters>
        </group>
      </parameter>
      <parameter>
        <label>(spacial unit / meter) factor (1000 for kilometers)</label>
        <float>
          <property required="no">
            <keyword>--xfactor=</keyword>
            <value/>
            <default>1000</default>
          </property>
        </float>
      </parameter>
      <parameter>
        <label>Working directory</label>
        <file directory="yes" filter-name="" filter-pattern="">
          <property required="yes">
            <keyword>--basedir=</keyword>
            <value/>
            <default>&lt;BASE&gt;</default>
          </property>
        </file>
      </parameter>
      <parameter>
        <label>Output directory name (inside working dir)</label>
        <string>
          <property required="yes">
            <keyword>--subdir=</keyword>
            <value/>
            <default>csmodel</default>
          </property>
        </string>
      </parameter>
      <parameter>
        <label>Quiet execution</label>
        <flag>
          <property required="no">
            <keyword>--quiet</keyword>
            <value>off</value>
            <default>off</default>
          </property>
        </flag>
      </parameter>
    </parameters>
  </program>
</flow>
