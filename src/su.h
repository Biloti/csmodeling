#ifndef SU_H
#define SU_H

/**
  \struct suheader_t
  Header de traços sísmicos, no formato do Seismic Unix.

  A descrição de cada campo pode ser encontrada no arquivo segy.h do Seismic Unix.
*/
typedef struct {
    int tracl;
    int tracr;
    int fldr;
    int tracf;
    int ep;
    int cdp;
    int cdpt;
    short trid;
    short nvs;
    short nhs;
    short duse;
    int offset;
    int gelev;
    int selev;
    int sdepth;
    int gdel;
    int sdel;
    int swdep;
    int gwdep;
    short scalel;
    short scalco;
    int sx;
    int sy;
    int gx;
    int gy;
    short counit;
    short wevel;
    short swevel;
    short sut;
    short gut;
    short sstat;
    short gstat;
    short tstat;
    short laga;
    short lagb;
    short delrt;
    short muts;
    short mute;
    short ns;
    short dt;
    short gain;
    short igc;
    short igi;
    short corr;
    short sfs;
    short sfe;
    short slen;
    short styp;
    short stas;
    short stae;
    short tatyp;
    short afilf;
    short afils;
    short nofilf;
    short nofils;
    short lcf;
    short hcf;
    short lcs;
    short hcs;
    short year;
    short day;
    short hour;
    short minute;
    short sec;
    short timbas;
    short trwf;
    short grnors;
    short grnofr;
    short grnlof;
    short gaps;
    short otrav;
    float d1;
    float f1;
    float d2;
    float f2;
    float ungpow;
    float unscale;
    int ntr;
    short mark;
    short shotpad;
    short unassigned[14];

} suheader_t;

int suheader_init (suheader_t * header);
#endif
