/*  csmodeling - CSHOT modeling wrapper
 *  Copyright (c) 2016-2021 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PALETTE_H
#define _PALETTE_H

/* One palette per line. First color is for rays, second is for velocity
   values, and others are for layers */

#ifdef CSHOT_BUG
/* Repeat third column once */

#define NCOLORS 8
const char palette[8][NCOLORS][7] = {
    {"000000", "000000", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF"},
    {"C00000", "000000", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF"},
    {"3A5952", "000000", "CBD4CE", "CBD4CE", "BBCCB3", "B2C08B", "B8B356", "D2A236"},
    {"D8D8D8", "FFFFFF", "AC8B52", "AC8B52", "2D0D00", "481100", "79201C", "863E2F"},
    {"483737", "000000", "FDDAC7", "FDDAC7", "FE9D5D", "FEE97D", "BDBD58", "F3EACE"},
    {"FFEEAA", "000000", "6D4E35", "6D4E35", "957256", "B19986", "716A65", "A69D98"},
    {"000000", "000000", "87615D", "87615D", "E77065", "D0A57C", "A2CBAA", "D7DBC4"},
    {"F4E3D7", "000000", "2D0100", "2D0100", "75130A", "D25922", "F69021", "FEBD2F"}
};

#else
#define NCOLORS 7
const char palette[8][NCOLORS][7] = {
    {"000000", "000000", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF"},
    {"C00000", "000000", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF", "FFFFFF"},
    {"3A5952", "000000", "CBD4CE", "BBCCB3", "B2C08B", "B8B356", "D2A236"},
    {"D8D8D8", "FFFFFF", "AC8B52", "2D0D00", "481100", "79201C", "863E2F"},
    {"483737", "000000", "FDDAC7", "FE9D5D", "FEE97D", "BDBD58", "F3EACE"},
    {"FFEEAA", "000000", "6D4E35", "957256", "B19986", "716A65", "A69D98"},
    {"000000", "000000", "87615D", "E77065", "D0A57C", "A2CBAA", "D7DBC4"},
    {"F4E3D7", "000000", "2D0100", "75130A", "D25922", "F69021", "FEBD2F"}
};

#endif

const int maxpalette = 7;

#endif
