/*  csmodeling - CSHOT modeling wrapper
 *  Copyright (c) 2016-2021 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CSHOT_H
#define _CSHOT_H

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>

typedef struct {
    int st0;
    int st1;
    int st2;
    int st3;

    int ntr;

    double sx;
    double sz;

} shot_t;

typedef struct {

    gdouble unit;               // unit/meter factor (1000 for kilometers, 1 for meters)
    gchar *title;               // title for plot (optional)

    /* interfaces */
    gint N;                     // number of interfaces
    gint *n;                    // vector of number of points for each interface
    gdouble **x;                // vector of x coords for each interface
    gdouble **z;                // vector of z coords for each interface
    gdouble *v;                 // vector of velocities below each interface

    gdouble xmin;               // bounding box for the model
    gdouble xmax;               //
    gdouble zmin;               //
    gdouble zmax;               //

    /* well */
    gdouble *wx;                // x coords for the well
    gdouble *wz;                // z coords for the well
    gint wn;                    // dimension of wx and wz

    gboolean direct;            // trace direct rays?
    gboolean primary;           // trace primary reflection rays?
    gboolean bottom;            // trace primary reflection at bottom interface?

    gchar **raycode;            // vector of futher ray codes

    gdouble takeoffangle;       // maximum takeoff angle
    gdouble takeoffangleinc;    // increment in takeoff angles

    /* first station */
    gint istation;              // index for the reference station
    gdouble xstation;           // x coordinate of the reference station
    gdouble dstation;           // distance between consecutive stations

    gdouble rdepth;             // depth of the receptors

    gchar **shotstr;            // list of shots (undecoded)
    gint nshotstr;              // number os shots strings
    shot_t *shot;               // list of shots (decoded)
    gint dxshot;                // distance between consecutive shots (when regular), in surface
    gdouble dwshot;             // arclength increment for shots in downhole acquisition
    gdouble sdepth;             // depth of the first source for downhole acquisition
    gint nshots;                // number of shots
    gboolean shot_regular;      // shot are regular?
    gboolean downhole;          // downhole acquisition?

    /* cshot 2 params */
    gint shot0;                 // first shot to build seismic section
    gint shot1;                 // last shot to build seismic section

    /* wavelet frenquencies */
    gdouble f0, f1, f2, f3;     // frequencies for the wavelet

    gdouble wlen;               // wavelet duration in time in seconds
    gint dt;                    // sampling rate in microseconds
    gdouble tmax;               // maximum recorded time


    /* export options */
    gchar *basedir;             // base dir to hold a directory for outputs
    gchar *subdir;              // directory name inside basedir

    gboolean gplt;              // generate figures with gnuplot?
    gint palette;               // palette for figure
    gint decimate;              // 1/decimate is the fraction of rays to plot
    gint raylimit;              // limit to number of rays in figure
    gboolean rays;              // plot rays?
    gboolean interfaces;        // plot interfaces?
    gboolean grid;              // show grid lines?
    gboolean showwell;          // show well?

    gdouble wxmin;              // bounding box for figure
    gdouble wxmax;              //
    gdouble wzmin;              //
    gdouble wzmax;              //
    gdouble xvel;               // x coordinate to velocity labels

    gboolean quiet;             // Report on screen?

} cshotp;

#endif /* _CSHOT_H */
