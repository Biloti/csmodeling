/*  csmodeling - CSHOT modeling wrapper
 *  Copyright (c) 2016-2021 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <math.h>

#include <glib/gstdio.h>

#include "parser.h"
#include "cshot.h"
#include "palette.h"
#include "su.h"

#ifdef CSHOT_BUG
#define FIRST_INTERFACE 2
#else
#define FIRST_INTERFACE 1
#endif

#define UNIT      ((p->unit == 1000 ? "km" : "m"))
#define UNITSTR   ((p->unit == 1000 ? "kilometers" : "meters"))
#define INPUTLINESIZE 120

void cshot_run (cshotp *p);
char *readline (char *string, FILE * infile);

int main (int argc, char **argv)
{
    cshotp *p;

    if ((p = parse_command_line (argc, argv)) == NULL)
        return EXIT_FAILURE;

    /* Let's run cshot */
    cshot_run (p);

    return EXIT_SUCCESS;
}

/* Print report with few statistics ------------------------------------------*/
void print_report (cshotp *p)
{

    int ii;

    fprintf (stderr, "\n******************************************\nCSMODELING REPORT\n\n");
    fprintf (stderr, "%-40s : %s/%s\n", "Working directory", p->basedir, p->subdir);

#ifdef CSHOT_BUG
    fprintf (stderr, "%-40s : %i\n", "Interfaces (including surface)", p->N - 1);
    fprintf (stderr, "%-40s : %s\n", "Unit for spatial coordinates", UNITSTR);
    fprintf (stderr, "%-40s : ", (p->unit == 1000 ? "Velocities (km/s)" : "Velocities (m/s)"));
    for (ii = 1; ii < p->N - 1; ii++)
        fprintf (stderr, "%.2f, ", p->v[ii]);
    fprintf (stderr, "%.2f\n", p->v[p->N - 1]);
#else
    fprintf (stderr, "%-40s : %i\n", "Interfaces (including surface)", p->N);
    fprintf (stderr, "%-40s : %s\n", "Unit for spatial coordinates", UNITSTR);
    fprintf (stderr, "%-40s : ", (p->unit == 1000 ? "Velocities (km/s)" : "Velocities (m/s)"));
    for (ii = 0; ii < p->N - 1; ii++)
        fprintf (stderr, "%.2f, ", p->v[ii]);
    fprintf (stderr, "%.2f\n", p->v[p->N - 1]);
#endif

    fprintf (stderr, "%-40s : [%.2f,%.2f] x [%.2f,%.2f]\n\n", "Model (approximate) bounding box", p->xmin, p->xmax, p->zmin, p->zmax);

    fprintf (stderr, "%-40s : %s\n", "Direct wave", (p->direct ? "yes" : "no"));
    fprintf (stderr, "%-40s : %s\n", "Primary reflections", (p->primary ? "yes" : "no"));

    if (p->primary)
        fprintf (stderr, "%-40s : %s\n", "Reflection at bottom interface", (p->bottom ? "yes" : "no"));

    fprintf (stderr, "\n%-40s : %.2f %s (%i)\n", "Reference-station x-coordinate / index", p->xstation, UNIT, p->istation);
    fprintf (stderr, "%-40s : %.4f %s\n", "Spacing between stations", p->dstation, UNIT);
    fprintf (stderr, "%-40s : %i -- %i\n", "Range of stations within the model",
             (int) floor ((p->xmin - p->xstation) / p->dstation + p->istation),
             (int) floor ((p->xmax - p->xstation) / p->dstation + p->istation));

    fprintf (stderr, "%-40s : %.4f %s\n\n", "Receivers' depth", p->rdepth, UNIT);

    /* Shots */
    fprintf (stderr, "%-40s : %s", "Shot location", (p->downhole ? "downhole" : "surface"));
    fprintf (stderr, "\n%-40s : %s\n", "Shot pattern", (p->shot_regular ? "regular" : "irregular"));
    fprintf (stderr, "%-40s : %i\n", "Number of shots", p->nshots);


    if (p->downhole == 0) {

        if (p->shot_regular) {
            fprintf (stderr, "%-40s : %.4f %s (%.1f) -- %.4f %s (%.1f)\n", "Shot location (station)",
                     st2x (p, p->shot[0].sx), UNIT,
                     p->shot[0].sx,
                     st2x (p, p->shot[0].sx + (p->nshots - 1) * p->dxshot), UNIT, p->shot[0].sx + (p->nshots - 1) * p->dxshot);

            fprintf (stderr, "%-40s : %.4f %s -- %.4f %s\n", "Offset range",
                     st2x (p, (double) p->shot[0].st0) - st2x (p, p->shot[0].sx), UNIT,
                     st2x (p, (double) p->shot[0].st3) - st2x (p, p->shot[0].sx), UNIT);

            if ((p->shot[0].st2 - p->shot[0].st1) > 1) {
                fprintf (stderr, "%-40s : %.4f -- %.4f\n", "Gap in offsets",
                         st2x (p, (double) (p->shot[0].st1 + 1)) - st2x (p, p->shot[0].sx),
                         st2x (p, (double) (p->shot[0].st2 - 1)) - st2x (p, p->shot[0].sx));
            } else {
                fprintf (stderr, "%-40s : no gap\n", "Gap in offsets");
            }

            fprintf (stderr, "%-40s : %i\n", "Traces per shot", p->shot[0].ntr);

            fprintf (stderr, "%-40s : %i\n\n", "Total number of traces", p->shot[0].ntr * p->nshots);
        } else {
            int ntr = 0;
            fprintf (stderr, "%-40s : %.4f %s (station = %.2f)\n", "First shot at", st2x (p, p->shot[0].sx), UNIT, p->shot[0].sx);
            fprintf (stderr, "%-40s : %.4f %s (station = %.2f)\n", "Last shot at",
                     st2x (p, p->shot[p->nshots - 1].sx), UNIT, p->shot[p->nshots - 1].sx);
            for (int ishot = 0; ishot < p->nshots; ishot++) {
                ntr += p->shot[ishot].ntr;
            }
            fprintf (stderr, "%-40s : %i\n\n", "Total number of traces", ntr);

        }
    } else {                    /* downhole */
        fprintf (stderr, "%-40s : %.4f\n", "Depth for first shot", p->sdepth);
        fprintf (stderr, "%-40s : %.4f\n\n", "Arclength increment for shots along well", p->dwshot);
    }

    fprintf (stderr, "%-40s : %i microseconds\n", "Sampling interval", p->dt);
    fprintf (stderr, "%-40s : %.1f seconds\n", "Recording time", p->tmax);
    fprintf (stderr, "%-40s : %i\n", "Samples per trace", (int) floor (p->tmax / (1.0e-6 * p->dt)) + 1);

    fprintf (stderr, "%-40s : %.1f : %.2f : %.1f degrees\n\n", "Takeoff angles for shooting rays",
             -p->takeoffangle, p->takeoffangleinc, p->takeoffangle);

    fprintf (stderr, "%-40s : out.bin\n", "Binary data file");
    fprintf (stderr, "%-40s : out.hdr\n", "Binary header");
    fprintf (stderr, "%-40s : cat out*.bin | supaste head=out.hdr ns=%i ftn=1 >out.su\n",
             "Command-line to convert to SU", (int) floor (p->tmax / (1.0e-6 * p->dt)) + 1);

    if (p->gplt) {
        fprintf (stderr, "%-40s : %s\n", "Title", (p->title == NULL ? "not defined" : p->title));

        fprintf (stderr, "%-40s : [%.2f,%.2f] x [%.2f,%.2f]\n", "Window for figure", p->wxmin, p->wxmax, p->wzmin, p->wzmax);
        fprintf (stderr, "%-40s : %s\n", "Gnuplot script to create PDF figure", "out.gp");
        fprintf (stderr, "%-40s : %s\n", "Datafile consumed by out.gp", "out.gdata");
        fprintf (stderr, "%-40s : %s\n", "Command-line to generate PDF & SVG", "gnuplot out.gp");
        fprintf (stderr, "%-40s : %s\n", "PDF file created", "out.pdf");
        fprintf (stderr, "%-40s : %s\n", "SVG file created", "out.svg");

        if (p->rays) {
            fprintf (stderr, "%-40s : %.1f%% (1/%i)\n", "Percentage of rays to plot", 100.0 / p->decimate, p->decimate);
        } else {
            fprintf (stderr, "%-40s : no rays ploted\n", "Percentage of rays to plot");
        }
    }
}

/* Make param1 file ----------------------------------------------------------*/
void mk_param1_file (cshotp *p)
{

    FILE *fp;
    int ii;

    fp = fopen ("param1", "w");

    fprintf (fp, "%-30s: interfaces\n", "model");
    fprintf (fp, "%i%-29s: number of interfaces in model file\n", p->N - 1, " ");
    fprintf (fp, "%-30s: color for ploting\n", "colors");

    if (p->wn > 0) {
        fprintf (fp, "%-30s: display the model and well\n", "mw");
        fprintf (fp, "%-30s: well log coordinates\n", "well");
    } else {
        fprintf (fp, "%-30s: display the model\n", "m");
        fprintf (fp, "%-30s: dummy well log coordinates\n", "well");
    }

    fprintf (fp, "%-30s: shooting mode (surface/downhole)\n", (p->downhole ? "d" : "s"));
    fprintf (fp, "%-30s: acquisition geometry\n", "geometry");
    fprintf (fp, "%-30s: plot sources and/or geophones\n", "");
    fprintf (fp, "%-30s: job descriptor (ray/listings/times)\n", "rlt");
    fprintf (fp, "%-30s: output filename\n", "out.");
    fprintf (fp, "% 6.2f  % 6.2f%-16s: range of takeoff angles\n", -p->takeoffangle, p->takeoffangle, " ");
    fprintf (fp, "% 6.2f%-24s: incremente in takeoff angle\n", p->takeoffangleinc, " ");

    fprintf (fp, "%12.4f\n", p->v[0]);
    for (ii = 1; ii < p->N - 1; ii++)
        fprintf (fp, "%12.4f\n", p->v[ii]);
    fprintf (fp, "%12.4f%-18s: last velocity\n", p->v[p->N - 1], " ");

    fprintf (fp, "%-30s: direct waves? (y/n)\n", (p->direct ? "y" : "n"));
    fprintf (fp, "%-30s: interfaces generating head waves\n", " ");
    fprintf (fp, "%-30s: primary reflections? (y/n)\n", "n");
    if (p->primary) {
        int last;

        last = (p->bottom ? p->N : p->N - 1);

        for (ii = FIRST_INTERFACE; ii < last; ii++)
            fprintf (fp, "%2i\n", ii);
    }
    if (p->raycode != NULL) {
        ii = 0;
        while (p->raycode[ii] != NULL) {
#ifdef CSHOT_BUG
            {                   /* interface indices other than surfaces have to be add by one */
                char *str;
                str = strtok (p->raycode[ii], " ");
                while (str != NULL) {
                    int idx;
                    idx = atoi (str);
                    fprintf (fp, " %i", (idx == 0 ? 0 : idx + 1));
                    str = strtok (NULL, " ");
                }
                fprintf (fp, "\n");
            }
#else
            fprintf (fp, "%s\n", p->raycode[ii]);
#endif
            ii++;
        }
    }

    fclose (fp);

}

/* Make model file -----------------------------------------------------------*/
void mk_model_file (cshotp *p)
{

    FILE *fp;
    int ii, jj;

    fp = fopen ("model", "w");

    /* Surface */
    for (jj = 0; jj < p->n[0]; jj++)
        fprintf (fp, "% 14.5f   % 14.5f\n", p->x[0][jj], p->z[0][jj]);
    fprintf (fp, "% 14.5f   % 14.5f\t: end of interface %i\n", 0.0, -99999.0, 0);

    for (ii = 1; ii < p->N; ii++) {
        for (jj = 0; jj < p->n[ii]; jj++)
            fprintf (fp, "% 14.5f   % 14.5f\n", p->x[ii][jj], p->z[ii][jj]);
        fprintf (fp, "% 14.5f   % 14.5f\t: end of interface %i\n", 0.0, -99999.0, ii);
    }

    fclose (fp);
}

/* Make color file -----------------------------------------------------------*/
void mk_color_file (void)
{
    FILE *fp;

    fp = fopen ("colors", "w");

    fprintf (fp, "0\treceivers\n");
    fprintf (fp, "0\tsources\n");
    fprintf (fp, "4\twell\n");
    fprintf (fp, "2\tcaustic rays\n");
    fprintf (fp, "2\trays\n");
    fprintf (fp, "0\tinterfaces\n\n");

    fclose (fp);
}

/* Make well file ------------------------------------------------------------*/
void mk_well_file (cshotp *p)
{

    FILE *fp;
    int ii;

    fp = fopen ("well", "w");

    if (p->downhole) {
        fprintf (fp, "% 14.5f   \t\t: x coordinate of well's top\n", p->wx[0]);
        for (ii = 0; ii < p->wn; ii++)
            fprintf (fp, "% 14.5f   % 14.5f\n", p->wx[ii], p->wz[ii]);
        fprintf (fp, "% 14.5f   % 14.5f\t: end of well\n", 0.0, -99999.0);
        fprintf (fp, "%14.5f\t\t\t: depth of the first source\n", p->sdepth);
        fprintf (fp, "%3i  %7.3f \t\t\t: # sources and source spacing along well\n", p->nshots, p->dwshot);
    } else {
        /* Dummy well */
        float top;

        top = (p->x[0][0] + p->x[0][p->n[0] - 1]) / 2;

        fprintf (fp, "% 14.5f   \t\t: x coordinate of dummy well's top\n", top);
        fprintf (fp, "% 14.5f   % 14.5f\n", top, p->z[1][0] / 2);
        fprintf (fp, "% 14.5f   % 14.5f\n", top, p->z[1][0]);
        fprintf (fp, "% 14.5f   % 14.5f\t: end of well\n", 0.0, -99999.0);
    }

    fclose (fp);
}

/* Make acquisition file -----------------------------------------------------*/
void mk_acquisition_file (cshotp *p)
{
    FILE *fp;
    int ii;

    fp = fopen ("geometry", "w");

    fprintf (fp, "%4i        % 12.4f        : reference station number and x-coord\n", p->istation, p->xstation);
    fprintf (fp, "%8.3f       %8.3f         : station spacing and receiver depth\n", p->dstation, p->rdepth);

    if (p->downhole) {
        if (p->shot_regular) {
            fprintf (fp, "%4i %4i %4i %4i             : receiver's geometry\n",
                     p->shot[0].st0, p->shot[0].st1, p->shot[0].st2, p->shot[0].st3);
        } else {
            for (ii = 0; ii < p->nshots; ii++)
                fprintf (fp, "%4i %4i %4i %4i             : receiver's geometry for shot %i\n",
                         p->shot[ii].st0, p->shot[ii].st1, p->shot[ii].st2, p->shot[ii].st3, ii + 1);
        }
    } else {
        if (p->shot_regular) {

            fprintf (fp, "%4i %4i %4i %4i %.2f %.2f : first shot geometry\n",
                     p->shot[0].st0, p->shot[0].st1, p->shot[0].st2, p->shot[0].st3, p->shot[0].sx, p->shot[0].sz);
            fprintf (fp, "%4i  %8.3f%-18s: total number of shots and displacement between them\n",
                     p->nshots, (p->dxshot) * (p->dstation), " ");
        } else {
            for (ii = 0; ii < p->nshots; ii++)
                fprintf (fp, "%4i %4i %4i %4i %.2f %.2f : shot %i\n",
                         p->shot[ii].st0, p->shot[ii].st1, p->shot[ii].st2, p->shot[ii].st3, p->shot[ii].sx, p->shot[ii].sz, ii + 1);
        }
    }

    fclose (fp);
}

/* Gnuplot prepare and run ---------------------------------------------------*/
int _gpl_interface_read (char *line, float **X, float **Z, int n, int nseg)
{
    int ii, jj;

    (*X) = malloc (sizeof (float) * n * nseg);
    (*Z) = malloc (sizeof (float) * n * nseg);

    for (ii = 0; ii < n; ii++) {
        line = strtok (NULL, "\n");
        sscanf (line, "%f %f", (*X) + ii, (*Z) + ii);
    }
    for (jj = 1; jj < nseg; jj++) {
        line = strtok (NULL, "\n");
        sscanf (line, "%*2i %*2i");

        for (ii = 0; ii < n; ii++) {
            line = strtok (NULL, "\n");
            sscanf (line, "%f %f", (*X) + ii + jj * n, (*Z) + ii + jj * n);
        }
    }
    return n * nseg;
}

char *_gpl_read_and_print_n_segments (int nseg, char *line, FILE * fp)
{
    float x, z;
    int npts, iseg, kk;

    for (iseg = 0; iseg < nseg; iseg++) {
        sscanf (line, "%2i %*2i", &npts);
        for (kk = 0; kk < npts; kk++) {
            line = strtok (NULL, "\n");
            sscanf (line, "%f %f", &x, &z);
            fprintf (fp, "% 12.6f   % 12.6f\n", x, z);
        }
        line = strtok (NULL, "\n");
    }
    fprintf (fp, "\n\n");

    return line;
}

char *_gpl_read_and_advance_n_segments (int nseg, char *line)
{
    float x, z;
    int npts, iseg, kk;

    for (iseg = 0; iseg < nseg; iseg++) {
        sscanf (line, "%2i %*2i", &npts);
        for (kk = 0; kk < npts; kk++) {
            line = strtok (NULL, "\n");
            sscanf (line, "%f %f", &x, &z);
        }
        line = strtok (NULL, "\n");
    }

    return line;
}

void gnuplot_run (cshotp *p, char *curves)
{
    char *line;
    int index;
    int nindex;
    float *zvel;

    FILE *gpfpd;
    FILE *gpfpc;

    int ii;
    int ret;
    int i0 = 0;
    int i1 = 0;

    gpfpd = fopen ("out.gdata", "w");

    if (!isnan (p->xvel))
        zvel = (float *) malloc (sizeof (float) * p->N);
    else
        zvel = NULL;

    /* Layers assembly */
    {
        int ilayer, inter;
        char *layers;
        float *X0, *Z0;
        float *X1, *Z1;
        int n0, n1;
        int N;

        X0 = NULL;
        Z0 = NULL;
        X1 = NULL;
        Z1 = NULL;

        layers = malloc (strlen (curves) + 1);
        memcpy (layers, curves, strlen (curves) + 1);

        line = layers;
        line = strtok (line, "\n");

        index = 0;
        inter = 0;

        N = p->N;
        for (ilayer = 1; ilayer <= N - 1; ilayer++) {

            if (X0 == NULL) {
                sscanf (line, "%2i %*2i", &n0);

                n0 = _gpl_interface_read (line, &X0, &Z0, n0, (p->n[inter] - 1));
                inter++;

                if (!isnan (p->xvel)) {
                    /* Compute z coordinate to velocity label over the interface */
                    if ((p->xvel < X0[0]) || (p->xvel > X0[n0 - 1]))
                        i0 = -1;
                    else {
                        i0 = 0;
                        while ((i0 < n0 - 2) && (p->xvel > X0[i0 + 1]))
                            i0++;
                    }
                }

                line = strtok (NULL, "\n");
            } else {            /* There is already two interfaces in memory */
                free (X0);
                free (Z0);

                X0 = X1;
                Z0 = Z1;
                n0 = n1;
                i0 = i1;
            }

            sscanf (line, "%2i %*2i", &n1);

            n1 = _gpl_interface_read (line, &X1, &Z1, n1, (p->n[inter] - 1));
            inter++;

            if (!isnan (p->xvel)) {
                float g;

                /* Compute z coordinate to velocity label over the interface */
                if ((p->xvel < X1[0]) || (p->xvel > X1[n1 - 1]))
                    i1 = -1;
                else {
                    i1 = 0;
                    while ((i1 < n1 - 2) && (p->xvel > X1[i1 + 1]))
                        i1++;
                }

                if ((i0 > 0) && (i1 > 0)) {

                    g = (X0[i0 + 1] - p->xvel) / (X0[i0 + 1] - X0[i0]);
                    zvel[ilayer] = g * Z0[i0] + (1 - g) * Z0[i0 + 1];

                    g = (X1[i1 + 1] - p->xvel) / (X1[i1 + 1] - X1[i1]);
                    zvel[ilayer] += g * Z1[i1] + (1 - g) * Z1[i1 + 1];

                    zvel[ilayer] = zvel[ilayer] / 2;
                } else
                    zvel[ilayer] = NAN;
            }

            fprintf (gpfpd, "# index %4i: layer %i\n", index, ilayer);

            for (ii = 0; ii < n0; ii++) {
                fprintf (gpfpd, "% 12.6f   % 12.6f\n", X0[ii], Z0[ii]);
            }
            fprintf (gpfpd, "% 12.6f   % 12.6f\n", X0[n0 - 1], p->wzmax);
            fprintf (gpfpd, "% 12.6f   % 12.6f\n", X0[0], p->wzmax);
            fprintf (gpfpd, "% 12.6f   % 12.6f\n\n\n", X0[0], Z0[0]);

            index++;
            line = strtok (NULL, "\n");
        }
        free (X0);
        free (Z0);
        free (X1);
        free (Z1);

        free (layers);
    }

    /* All curves (from begining) */
    line = curves;
    strtok (line, "\n");

    /* Interfaces first */
    for (ii = 0; ii < p->N; ii++) {
        if (p->interfaces) {
            fprintf (gpfpd, "# index %4i: interface %i\n", index, ii);
            line = _gpl_read_and_print_n_segments (p->n[ii] - 1, line, gpfpd);
            index++;
        } else {
            line = _gpl_read_and_advance_n_segments (p->n[ii] - 1, line);
        }
    }

    /* Well */
    if (p->downhole) {
        if (p->showwell) {
            fprintf (gpfpd, "# index %4i: well\n", index);
            line = _gpl_read_and_print_n_segments (p->wn, line, gpfpd);
            index++;
        } else
            line = _gpl_read_and_advance_n_segments (p->wn, line);
    }

    /* Rays */
    int iray = 0;
    int eray = 0;

    while (p->rays && line != NULL && eray < p->raylimit) {
        div_t c;

        c = div (iray, p->decimate);

        if (c.rem == 0) {
            fprintf (gpfpd, "# index %4i: ray %i\n", index, ++iray);
            line = _gpl_read_and_print_n_segments (1, line, gpfpd);
            eray++;
            index++;
        } else {
            iray++;
            line = _gpl_read_and_advance_n_segments (1, line);
        }
    }
    fclose (gpfpd);

    nindex = index;

    if (!p->quiet)
        fprintf (stderr, "%-40s : %i\n", "Elements ploted", nindex);

    /* out.gp (gnuplot script file) ------------------------------------------ */
    index = 0;
    gpfpc = fopen ("out.gp", "w");

    fprintf (gpfpc, "set terminal pdfcairo size 15cm,8cm font 'Arial,14'\n");
    fprintf (gpfpc, "set output \"out.pdf\"\n\nset key off\n\n");

    fprintf (gpfpc, "# Style for interfaces\n");
    fprintf (gpfpc, "set style line 100 lw 1.5 lc rgb \"#000000\"\n\n");

    fprintf (gpfpc, "# Style for well\n");
    fprintf (gpfpc, "set style line 101 lw 2.0 lc rgb \"#AF0000\"\n\n");

    fprintf (gpfpc, "# Style for rays\n");
    fprintf (gpfpc, "set style line 102 lw 0.5 lc rgb \"#%s\"\n\n", palette[p->palette][0]);

    fprintf (gpfpc, "# Style for major and minor grid lines\n");
    fprintf (gpfpc, "set style line 120 lw 0.5 lt 0 lc rgb \"#000000\"\n");
    fprintf (gpfpc, "set style line 121 lw 0.5 lt 0 lc rgb \"#000000\"\n\n");

    fprintf (gpfpc, "# Style for layers\n");
    fprintf (gpfpc, "set style fill noborder\n");
    for (ii = 0; ii < p->N; ii++) {
        div_t c;
        c = div (ii, NCOLORS - 2);

        fprintf (gpfpc, "set style line %i lw 1.0 lc rgb \"#%s\"\n", 200 + ii, palette[p->palette][c.rem + 2]);
    }

    if (!isnan (p->xvel)) {
        fprintf (gpfpc, "\n");

        for (ii = 0; ii < p->N - 1; ii++) {
            if (!isnan (zvel[ii + 1])) {
                if (p->unit == 1000) {
                    fprintf (gpfpc, "set label %i \"%.3f km/s\" at %.2f,%.2f tc rgb \"#%s\" front\n",
                             ii + 1, p->v[ii], p->xvel, zvel[ii + 1], palette[p->palette][1]);
                } else if (p->unit == 1) {
                    fprintf (gpfpc, "set label %i \"%.0f m/s\" at %.2f,%.2f tc rgb \"#%s\" front\n",
                             ii + 1, p->v[ii], p->xvel, zvel[ii + 1], palette[p->palette][1]);
                } else {
                    fprintf (gpfpc, "set label %i \"%.3f\" at %.2f,%.2f tc rgb \"#%s\" front\n", ii + 1, p->v[ii], p->xvel,
                             zvel[ii + 1], palette[p->palette][0]);
                }
            }
#ifdef CSHOT_BUG
            ii = (ii == 0 ? 1 : ii);
#endif
        }

    }

    if (p->title != NULL) {
        fprintf (gpfpc, "\nset title \"%s\"", p->title);
    }

    if (p->unit == 1000) {
        fprintf (gpfpc, "\nset xlabel \"Distance [km]\"\n");
        fprintf (gpfpc, "set ylabel \"Depth [km]\"\n\n");
    } else if (p->unit == 1) {
        fprintf (gpfpc, "\nset xlabel \"Distance [m]\"\n");
        fprintf (gpfpc, "set ylabel \"Depth [m]\"\n\n");
    } else {
        fprintf (gpfpc, "\nset xlabel \"Distance\"\n");
        fprintf (gpfpc, "set ylabel \"Depth\"\n\n");
    }

    fprintf (gpfpc, "set xrange [% 8.3f:% 8.3f]\nset yrange [% 8.3f:% 8.3f]\n\n", p->wxmin, p->wxmax, p->wzmax, p->wzmin);

    if (p->grid) {
        fprintf (gpfpc, "set grid front\n");
        fprintf (gpfpc, "set grid xtics ls 120, ls 121\n");
        fprintf (gpfpc, "set grid ytics ls 120, ls 121\n");
        fprintf (gpfpc, "set grid mxtics\n");
        fprintf (gpfpc, "set grid mytics\n");
        fprintf (gpfpc, "set mxtics 2\n");
        fprintf (gpfpc, "set mytics 2\n\n");
    }

    /* N Layers: 0,...,N-2 */
    fprintf (gpfpc, "plot \"out.gdata\" index 0 w filledcurves ls 200");
    index++;

#ifdef CSHOT_BUG
    index++;
    for (ii = 2; ii <= p->N - 2; ii++, index++)
        fprintf (gpfpc, ", \\\n     \"\" index %i w filledcurves ls %i", ii, 200 + ii);
#else
    for (ii = 1; ii <= p->N - 2; ii++, index++)
        fprintf (gpfpc, ", \\\n     \"\" index %i w filledcurves ls %i", ii, 200 + ii);
#endif

    /* Interfaces: (0,...,N-1) */
    if (p->interfaces) {
        for (ii = 0; ii < p->N; ii++) {

#ifdef CSHOT_BUG
            if (ii != 1)        /* Work-around: skip interface 1 */
                fprintf (gpfpc, ", \\\n     \"\" index %i w l ls 100", index);
#else
            fprintf (gpfpc, ", \\\n     \"\" index %i w l ls 100", index);
#endif
            index++;
        }
    }

    /* Well */
    if (p->wn > 0 && p->showwell) {
        fprintf (gpfpc, ", \\\n     \"\" index %i w l ls 101", index);
        index++;
    }

    /* Rays all remaining sets: */
    if (p->rays) {

        while (index < nindex) {
            fprintf (gpfpc, ", \\\n     \"\" index %i w l ls 102", index);
            index++;
        }
    }

    fprintf (gpfpc, "\n\n");

    fprintf (gpfpc, "set terminal svg size 531.5,283.5\n");
    fprintf (gpfpc, "set output \"out.svg\"\n\nreplot\n");

    fclose (gpfpc);

    ret = system ("gnuplot out.gp");
    if (ret) {
        fprintf (stderr, "ERROR: problem with gnuplot\n");
        exit (EXIT_FAILURE);
    }

    free (zvel);
}


/* Make param2 file ----------------------------------------------------------*/
void mk_param2_file (cshotp *p, int ishotini, int ishotend)
{
    FILE *fp;
    char str[15];

    fp = fopen ("param2", "w");

    fprintf (fp, "%-30s: shot/receiver gathers (s/r)\n", "s");
    fprintf (fp, "%3i %3i %-22s: first and last shot to process\n", ishotini, ishotend, " ");
    fprintf (fp, "%3i %3i %-22s: first and last trace to process (take from 1st shot)\n", 1, p->shot[ishotini - 1].ntr, " ");
    fprintf (fp, "%4.1f  %4.1f  %4.1f  %4.1f %-7s: source frequencies\n", p->f0, p->f1, p->f2, p->f3, " ");
    fprintf (fp, "%5.3f %-24s: Wavelet length (seconds)\n", p->wlen, " ");
    fprintf (fp, "%5.3f %-24s: Sampling rate (seconds)\n", p->dt * 1.0e-6, " ");
    fprintf (fp, "%5.3f %-24s: Maximum recorded time (seconds)\n", p->tmax, " ");
    fprintf (fp, "%-30s: Input file\n", "out.shot");
    sprintf (str, "out-%04i.bin", ishotini);
    fprintf (fp, "%-30s: Binary output file\n", str);

    fclose (fp);
}

/* Make su header file ----------------------------------------------------------*/
void mk_su_header (cshotp *p)
{

    /* header-words written:
       dt, tracl, tracr, tracf, scael, scalco, sx, sdepth, gx, gelev, offset
     */

    FILE *fp;
    int ii, st;
    int unit;
    suheader_t hdr;

    suheader_init (&hdr);

    fp = fopen ("out.hdr", "w");

    hdr.dt = p->dt;
    hdr.tracl = 1;
    hdr.tracr = 1;
    hdr.trid = 1;
    hdr.scalel = -100;
    hdr.scalco = -100;
    hdr.ns = (int) floor (p->tmax / (1.0e-6 * p->dt)) + 1;
    unit = p->unit * 100;

    if (p->downhole == 0) {

        if (p->shot_regular) {
            for (ii = 0; ii < p->nshots; ii++) {
                hdr.sx = (int) unit *st2x (p, p->shot[0].sx + ii * p->dxshot);
                hdr.sdepth = (int) unit *p->shot[0].sz;

                hdr.tracf = 1;
                hdr.gelev = (int) -unit * p->rdepth;

                for (st = p->shot[0].st0; st <= p->shot[0].st1; st++) {
                    hdr.gx = (int) unit *st2x (p, st + ii * p->dxshot);
                    hdr.offset = hdr.gx - hdr.sx;
                    fwrite (&hdr, sizeof (suheader_t), 1, fp);
                    hdr.tracl++;
                    hdr.tracr++;
                    hdr.tracf++;
                }

                for (st = p->shot[0].st2; st <= p->shot[0].st3; st++) {
                    hdr.gx = (int) unit *st2x (p, st + ii * p->dxshot);
                    hdr.offset = hdr.gx - hdr.sx;

                    fwrite (&hdr, sizeof (suheader_t), 1, fp);
                    hdr.tracl++;
                    hdr.tracr++;
                    hdr.tracf++;
                }
            }
        } else {
            for (ii = 0; ii < p->nshots; ii++) {

                hdr.sx = (int) unit *st2x (p, p->shot[ii].sx);
                hdr.sdepth = (int) unit *p->shot[ii].sz;

                hdr.tracf = 1;
                hdr.gelev = (int) -unit * p->rdepth;
                for (st = p->shot[ii].st0; st <= p->shot[ii].st1; st++) {
                    hdr.gx = (int) unit *st2x (p, (double) st);
                    hdr.offset = hdr.gx - hdr.sx;
                    fwrite (&hdr, sizeof (suheader_t), 1, fp);
                    hdr.tracl++;
                    hdr.tracr++;
                    hdr.tracf++;
                }
                for (st = p->shot[ii].st2; st <= p->shot[ii].st3; st++) {
                    hdr.gx = (int) unit *st2x (p, (double) st);
                    hdr.offset = hdr.gx - hdr.sx;
                    fwrite (&hdr, sizeof (suheader_t), 1, fp);
                    hdr.tracl++;
                    hdr.tracr++;
                    hdr.tracf++;
                }
            }
        }
    } else {                    /*downhole */

        char lstfname[1024];
        char line[INPUTLINESIZE];
        FILE *lst;

        sprintf (lstfname, "%s/%s/out.listing", p->basedir, p->subdir);
        lst = fopen (lstfname, "r");

        /* Skip first 12 lines */
        for (int i = 0; i < 11; i++) {
            readline (line, lst);
        }

        hdr.gelev = (int) -unit * p->rdepth;

        /* Read shot postions */
        for (int i = 0; i < p->nshots; i++) {
            float sx, sdepth;
            readline (line, lst);
            sscanf (line, "%*i %f %f %*i", &sx, &sdepth);
            hdr.sx = (int) unit *sx;
            hdr.sdepth = (int) unit *sdepth;
            hdr.tracf = 1;
            for (st = p->shot[0].st0; st <= p->shot[0].st1; st++) {
                hdr.gx = unit * st2x (p, st);
                hdr.offset = hdr.gx - hdr.sx;

                fwrite (&hdr, sizeof (suheader_t), 1, fp);
                hdr.tracl++;
                hdr.tracr++;
                hdr.tracf++;
            }
            for (st = p->shot[0].st2; st <= p->shot[0].st3; st++) {
                hdr.gx = (float) unit *st2x (p, st);
                hdr.offset = hdr.gx - hdr.sx;

                fwrite (&hdr, sizeof (suheader_t), 1, fp);
                hdr.tracl++;
                hdr.tracr++;
                hdr.tracf++;
            }
        }
        fclose (lst);
    }

    fclose (fp);
}


/* Run cshot1, gnuplot and cshot 2 -------------------------------------------*/
void cshot_run (cshotp *p)
{
    GString *dir;
    GError *error;

    char *std_out;
    char *comments;

    gboolean status;
    int exitstatus;

    if (!p->quiet)
        print_report (p);

    /* Setup of the working directory ------------------------------------------ */
    if (p->basedir == NULL) {
        fprintf (stderr, "ERROR: Basedir is required.\n");
        exit (EXIT_FAILURE);
    }

    dir = g_string_new (p->basedir);

    if (p->subdir != NULL) {

        dir = g_string_append (dir, "/");
        dir = g_string_append (dir, p->subdir);

        if (g_file_test (dir->str, G_FILE_TEST_EXISTS)) {
            if (!g_file_test (dir->str, G_FILE_TEST_IS_DIR)) {
                fprintf (stderr, "ERROR: %s exists but is not a directory.\n", dir->str);
                exit (EXIT_FAILURE);
            }
        } else {
            if (g_mkdir (dir->str, 0700) != 0) {
                fprintf (stderr, "ERROR: unable to create %s.\n", dir->str);
                exit (EXIT_FAILURE);
            }
        }
    } else {
        g_string_append (dir, "/csm-XXXXXX");

        if (g_mkdtemp (dir->str) == NULL) {
            fprintf (stderr, "Unable to create working directory.\n");
            exit (EXIT_FAILURE);
        }
    }
    g_chdir (dir->str);

    /* make cshot1 parameter files --------------------------------------------- */
    mk_param1_file (p);
    mk_model_file (p);
    mk_color_file ();
    mk_well_file (p);
    mk_acquisition_file (p);

    /* everything in place, running cshot1 ------------------------------------- */
    status = g_spawn_command_line_sync ("cshot1", &std_out, &comments, &exitstatus, &error);
    if (status == FALSE) {
        fprintf (stderr, "Failure in calling cshot1.\n");
        exit (EXIT_FAILURE);
    } else {
        FILE *fp;

        fp = fopen ("out.cshot1", "w");
        fprintf (fp, "%s\n", comments);
        fclose (fp);
        free (comments);
    }

    /* process curves to prepare for gnuplot and run it ------------------------ */
    if (p->gplt) {
        gnuplot_run (p, std_out);
        free (std_out);
    }
    /* param2 ------------------------------------------------------------------ */

    if (p->shot_regular) {
        mk_param2_file (p, p->shot0, p->shot1);
        /* everything in place, running cshot2 ------------------------------------- */
        status = g_spawn_command_line_sync ("cshot2", &std_out, &comments, &exitstatus, &error);
        if (status == FALSE) {
            fprintf (stderr, "Failure in calling cshot2.\n");
            exit (EXIT_FAILURE);
        } else {
            FILE *fp;

            fp = fopen ("out.cshot2", "w");
            fprintf (fp, "%s\n", comments);
            fclose (fp);
            free (std_out);
            free (comments);
        }
    } else {
        for (int ishot = p->shot0; ishot <= p->shot1; ishot++) {
            mk_param2_file (p, ishot, ishot);
            /* everything in place, running cshot2 ------------------------------------- */
            status = g_spawn_command_line_sync ("cshot2", &std_out, &comments, &exitstatus, &error);
            if (status == FALSE) {
                fprintf (stderr, "Failure in calling cshot2.\n");
                exit (EXIT_FAILURE);
            } else {
                FILE *fp;

                fp = fopen ("out.cshot2", "w");
                fprintf (fp, "%s\n", comments);
                fclose (fp);
                free (std_out);
                free (comments);
            }
        }
    }

    /* create a binary header file, suitable for sushw ------------------------- */
    mk_su_header (p);
    g_string_free (dir, TRUE);
}

/* Read one line from a text file  -------------------------------------------*/
char *readline (char *string, FILE * infile)
{
    char *result;

    /* Search for something that looks like a number. */
    do {
        result = fgets (string, INPUTLINESIZE, infile);
        if (result == (char *) NULL) {
            printf ("  Error:  Unexpected end of file\n");
            exit (1);
        }
        /* Skip anything that doesn't look like a number, a comment, */
        /*   or the end of a line.                                   */
        while ((*result != '\0') && (*result != '#')
               && (*result != '.') && (*result != '+') && (*result != '-')
               && ((*result < '0') || (*result > '9'))) {
            result++;
        }
        /* If it's a comment or end of line, read another line and try again. */
    } while ((*result == '#') || (*result == '\0'));
    return result;
}
