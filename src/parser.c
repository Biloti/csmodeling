/*  csmodeling - CSHOT modeling wrapper
 *  Copyright (c) 2016-2021 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <glib.h>

#include "cshot.h"
#include "parser.h"

extern int maxpalette;

#define CSMOD_VERSION "2021.06.22"

#ifndef max
#define max(x,y)         ( ((x) < (y)) ? (y) : (x) )
#endif

#ifndef min
#define min(x,y)         ( ((x) > (y)) ? (y) : (x) )
#endif

#define check_required(xx,yy)       if (xx == NULL){fprintf(stderr, "%s required.\n", yy); return EXIT_FAILURE;}
#define convert_double(aa,bb,nn,cc) { check_required(aa,cc);    \
    if (list_to_double (aa, bb, nn) != (nn)){                   \
      fprintf(stderr, "%s should have %i elements.\n", cc, nn); \
      return EXIT_FAILURE; }}

int list_to_double (char *list, double *values, int max);
int fill_in_cshotp (cshotp *p, struct parse_params *pp);

/* Convert station index to coordinate ---------------------------------------*/
double st2x (cshotp *p, double st)
{
    return (st - p->istation) * p->dstation + p->xstation;
}

/* Check if a number is within a range ---------------------------------------*/
int isinrange (double x, double a, double b)
{

    return ((a < x) && (x < b));

}

cshotp *parse_command_line (int argc, char **argv)
{
    static struct parse_params pp;
    static cshotp p;
    static int show_version;

    static GOptionEntry acquisition_entries[] = {
        {"direct", 'd', 0, G_OPTION_ARG_NONE, &p.direct, "enable direct wave computation", NULL},
        {"primary", 'p', 0, G_OPTION_ARG_NONE, &p.primary, "enable primary reflection computation", NULL},
        {"bottom", 0, 0, G_OPTION_ARG_NONE, &p.bottom, "enable reflection at bottom interface", NULL},
        {"raycode", 'w', 0, G_OPTION_ARG_STRING_ARRAY, &p.raycode,
         "ray code (list of interface where\n                                          the ray reflects)", "\"i1 i2 ... ik\""},
        {"takeoff", 'a', 0, G_OPTION_ARG_DOUBLE, &p.takeoffangle, "takeoff angle (degrees)", "80"},
        {"inc", 0, 0, G_OPTION_ARG_DOUBLE, &p.takeoffangleinc, "takeoff angle increment (degrees)", "1"},
        {"xstation", 0, 0, G_OPTION_ARG_DOUBLE, &p.xstation, "first station x-coordinate", "1.0"},
        {"istation", 0, 0, G_OPTION_ARG_INT, &p.istation, "reference number of the first station", "1"},
        {"dstation", 0, 0, G_OPTION_ARG_DOUBLE, &p.dstation, "station spacing", "0.05"},
        {"rdepth", 0, 0, G_OPTION_ARG_DOUBLE, &p.rdepth, "receiver depth", "0.0"},
        {"well", 0, 0, G_OPTION_ARG_STRING, &pp.well, "(x,z) knots for well", "\"x1,z1; x2,z2; ...; xn,zn\""},
        {"shot", 0, 0, G_OPTION_ARG_STRING_ARRAY, &p.shotstr,
         "active range of stations for\n                                          a shot in (sx,sz)", "st1,st2,st3,st4,sx,sz"},
        {"dxshot", 0, 0, G_OPTION_ARG_INT, &p.dxshot,
         "shot increment, in stations,\n                                          for surface acquisition", NULL},
        {"dwshot", 0, 0, G_OPTION_ARG_DOUBLE, &p.dwshot,
         "distance between shots, for downhole\n                                          acquisition", NULL},
        {"sdepth", 0, 0, G_OPTION_ARG_DOUBLE, &p.sdepth,
         "depth of the first source in case\n                                          of downhole acquisition", NULL},
        {"nshots", 0, 0, G_OPTION_ARG_INT, &p.nshots, "number of regular shots", "1"},
        {NULL}
    };

    static GOptionEntry seis_entries[] = {
        {"f0", 0, 0, G_OPTION_ARG_DOUBLE, &p.f0, "first wavelet frequency", "10"},
        {"f1", 0, 0, G_OPTION_ARG_DOUBLE, &p.f1, "wavelet frequency", "25"},
        {"f2", 0, 0, G_OPTION_ARG_DOUBLE, &p.f2, "wavelet frequency", "35"},
        {"f3", 0, 0, G_OPTION_ARG_DOUBLE, &p.f3, "last wavelet frequency", "50"},
        {"wlen", 0, 0, G_OPTION_ARG_DOUBLE, &p.wlen, "wavelet length (seconds)", "0.150"},
        {"dt", 0, 0, G_OPTION_ARG_INT, &p.dt, "sampling rate in (micro-seconds)", "4000"},
        {"tmax", 'T', 0, G_OPTION_ARG_DOUBLE, &p.tmax, "Maximum recorded time (seconds)", "4.0"},
        {NULL}
    };

    static GOptionEntry export_entries[] = {
        {"nogplt", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &p.gplt,
         "suppress model/rays export\n                                          through gnuplot", NULL},
        {"title", 0, 0, G_OPTION_ARG_STRING, &p.title, "title for figure", NULL},
        {"rays", 0, 0, G_OPTION_ARG_NONE, &p.rays, "plot rays", NULL},
        {"decimate", 0, 0, G_OPTION_ARG_INT, &p.decimate, "plot one ray at every n", "n"},
        {"limit", 0, 0, G_OPTION_ARG_INT, &p.raylimit, "plot the first N rays only", "N"},
        {"nointerfaces", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &p.interfaces, "suppress interfaces in figures", NULL},
        {"nowell", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &p.showwell, "suppress well in figures", NULL},
        {"nogrid", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &p.grid, "suppress grid lines", NULL},
        {"xmin", 0, 0, G_OPTION_ARG_DOUBLE, &p.wxmin, "minumum x coordinate in figure", NULL},
        {"xmax", 0, 0, G_OPTION_ARG_DOUBLE, &p.wxmax, "maximum x coordinate in figure", NULL},
        {"zmin", 0, 0, G_OPTION_ARG_DOUBLE, &p.wzmin, "minumum z coordinate in figure", NULL},
        {"zmax", 0, 0, G_OPTION_ARG_DOUBLE, &p.wzmax, "maximum z coordinate in figure", NULL},
        {"xvel", 0, 0, G_OPTION_ARG_DOUBLE, &p.xvel, "x coordinate to display\n                                          layer velocities",
         NULL},
        {"palette", 'c', 0, G_OPTION_ARG_INT, &p.palette, "palette for graphics (0,1,...,7)", "2"},
        {NULL}
    };

    static GOptionEntry entries[] = {
        {"xfactor", 0, 0, G_OPTION_ARG_DOUBLE, &p.unit,
         "(spacial unit)/ meters factor\n                                          (1000 for km, 1 for m or 0 for autodetect)", "0"},
        {"knots", 'k', 0, G_OPTION_ARG_STRING_ARRAY, &pp.knots, "interface's knots", "\"x1,z1;x2,z2;...;xn,zn\""},
        {"velocity", 'v', 0, G_OPTION_ARG_STRING, &pp.v, "layer's velocities", "v1,v2,...,vN"},
        {"basedir", 0, 0, G_OPTION_ARG_FILENAME, &p.basedir, "working directory", "<path>"},
        {"subdir", 0, 0, G_OPTION_ARG_STRING, &p.subdir, "output directory inside working dir", NULL},
        {"version", 'V', 0, G_OPTION_ARG_NONE, &show_version, "show csmodeling version", NULL},
        {"quiet", 0, 0, G_OPTION_ARG_NONE, &p.quiet, "run quietly", NULL},
        {NULL}
    };

    GError *error = NULL;
    GOptionContext *parser;
    GOptionGroup *group;

    /* Set a short description for the program */
    parser = g_option_context_new ("- CSHOT modeling wrapper");

    /* Summary */
    g_option_context_set_summary (parser,
                                  "This is a wrapper to Cshot modeling tool, which is designed to\n"
                                  "simulate a 2D data aquisition, over an acoustic homogeneous-layered\n" "model, through ray tracing.");

    /* Description */
    g_option_context_set_description (parser,
                                      "Csmodeling, being a wrapper to Cshot, models true-amplitude \n"
                                      "shot data in 2.5D layered acoustic media. Each layer is \n"
                                      "homogeneous.\n\n"
                                      "The interfaces are prescribed as knots of an interpolant cubic\n"
                                      "spline, from right to left, and from top interface to bottom.\n"
                                      "This means that only smooth interfaces are allowed.\n\n"
                                      "For example, an interface with four knots would be specified as:\n"
                                      "   --knots=\"0.0,1.2; 2.4,1.3; 5.0,1.3; 8.0,1.5\"\n"
                                      "As you may guess, each knot is a pair x,z, and knots are sperated\n"
                                      "by semicolon. In this example, coordinates were prescribed in\n"
                                      "kilometers. Meters can be used as well.\n\n"
                                      "First step to setup an acquisition is the prescription of the\n"
                                      "stations. They are regularly distributed along the surface.\n"
                                      "For example, over a model that extends for 5km, stations could be\n"
                                      "positionated from the begining of the model at every 20m.\n\n"
                                      "That would be accomplished by:\n"
                                      "   --istation=0 --xstation=0 --dstation=0.02\n"
                                      "As reference, the station located at 0 was labeled as station 0.\n\n"
                                      "Every shot is prescribed by receiver's locations, determined by\n"
                                      "the initial station reference number (or station index), last\n"
                                      "station index before a gap, first station index after a gap, and last\n"
                                      "station index. Also the shot location is provided in terms of the\n"
                                      "station it occupies and its depth. However, shots can be allocated\n"
                                      "between stations, by providing a float station index. For example, a\n"
                                      "shot specified as\n"
                                      "   --shot=\"50,150,170,270,160.0,0.015\"\n"
                                      "means that the shot is located at station 160, buried (15m), and\n"
                                      "the receivers are located from station 50 up to 150, and from station\n"
                                      "170 up to 270. Therefore, there is a gap in the acquisition.\n\n"
                                      "Multiple shots can be specified, or a single shot and a pattern to\n"
                                      "to shift it regularly along the acquisition line.\n\n"
                                      "Direct waves (--direct or -d) and primary waves (--primary or -p)\n"
                                      "can be modeled. Other waves can also be modeled by prescribing their\n"
                                      "sequence of interfaces where reflections occurs. For example\n "
                                      "   --raycode=\"2 0 1\"\n"
                                      "describes rays that propagate down from source, through first and\n"
                                      "second layers, reflect at second interface, propagate back to surface,\n"
                                      "and down to finally reflect at first interface before been\n"
                                      "recorded at the receptors. Schematically, this ray code can be\n"
                                      "represented as\n"
                                      "   --S--------R-----     surface\n"
                                      "      \\    /\\/           layer 1\n"
                                      "   -----------------     interface 1\n"
                                      "        \\/               layer 2\n"
                                      "   -----------------     interface 2\n\n"
                                      "Copyright (c) 2016-2021 Ricardo Biloti <biloti@unicamp.br>\n"
                                      "Department of Applied Mathematics, IMECC, UNICAMP -- Brazil\n");


    group = g_option_group_new ("geom", "Acquisition geometry parameters:", "Show aquisition geometry parameters", NULL, NULL);
    g_option_group_add_entries (group, acquisition_entries);
    g_option_context_add_group (parser, group);

    group = g_option_group_new ("seis", "Seismogram parameters:", "Show synthetic seismogram parameters", NULL, NULL);
    g_option_group_add_entries (group, seis_entries);
    g_option_context_add_group (parser, group);

    group = g_option_group_new ("export", "Export parameters:", "Show export parameters", NULL, NULL);
    g_option_group_add_entries (group, export_entries);
    g_option_context_add_group (parser, group);

    g_option_context_add_main_entries (parser, entries, NULL);

    /* Complain about unknown options */
    g_option_context_set_ignore_unknown_options (parser, FALSE);

    /* Default values */
    p.unit = 0;                 /* Undetermined */

    p.istation = 1;
    p.xstation = 1.0;
    p.rdepth = 0.0;
    p.dstation = 0.05;

    p.primary = FALSE;
    p.bottom = FALSE;
    p.raycode = NULL;

    p.takeoffangle = 80;
    p.takeoffangleinc = 1;

    p.f0 = 10;
    p.f1 = 25;
    p.f2 = 35;
    p.f3 = 50;

    p.nshots = 1;
    p.dxshot = 0;
    p.dwshot = 0;
    p.shot_regular = TRUE;
    p.sdepth = NAN;

    p.shot0 = 1;
    p.shot1 = -1;

    p.wlen = 0.15;
    p.dt = 4000;
    p.tmax = 4;

    p.palette = 2;
    p.decimate = 1;
    p.raylimit = INT_MAX;

    p.gplt = TRUE;
    p.title = NULL;
    p.rays = FALSE;
    p.grid = TRUE;
    p.interfaces = TRUE;
    p.showwell = TRUE;

    p.wxmin = NAN;
    p.wxmax = NAN;
    p.wzmin = NAN;
    p.wzmax = NAN;

    p.xvel = NAN;

    pp.well = NULL;
    p.wn = 0;

    p.quiet = FALSE;
    show_version = FALSE;

    /* Parse command line */
    if (g_option_context_parse (parser, &argc, &argv, &error) == FALSE) {
        fprintf (stderr, "%s: syntax error\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (show_version) {
        fprintf (stderr, "csmodeling version %s\n", CSMOD_VERSION);
        return NULL;
    }

    if (p.shot1 < p.shot0) {
        p.shot1 = p.nshots;
    }

    if (p.shotstr == NULL) {
        fprintf (stderr, "> ERROR: At least one shot must be specified.\n");
        exit (EXIT_FAILURE);
    }

    /* Compute the number of shots in case it was not provided */
    p.nshotstr = 0;
    while (p.shotstr[p.nshotstr] != NULL)
        p.nshotstr++;

    if (p.nshotstr > 1) {
        /* irregular acquisition */
        p.shot_regular = FALSE;
        p.nshots = p.nshotstr;
        p.shot1 = p.nshots;
    }

    /* Downhole acquisition? */
    p.downhole = (pp.well != NULL);
    if (p.downhole && isnan (p.sdepth)) {
        fprintf (stderr, "> ERROR: first source's depth mut be specified when acquiring downhole.\n");
        exit (EXIT_FAILURE);
    }

    p.palette = max (0, p.palette);
    p.palette = min (p.palette, maxpalette);

    p.decimate = max (p.decimate, 1);

    g_option_context_free (parser);

    /* Filling in vectors */
    if (fill_in_cshotp (&p, &pp))
        return NULL;

    /* Infer units, in case not specified */
    if (p.unit == 0) {
        gdouble aux = log (p.xmax) / log (10);
        p.unit = (aux < 3 ? 1000 : 1);
    }

    return &p;
}

int list_to_double (char *list, double *values, int max)
{
    int n = 0;
    char *strvalue;

    strvalue = strtok (list, ",");
    values[n++] = atof (strvalue);

    while ((strvalue = strtok (NULL, ",")) != NULL && n < max)
        values[n++] = atof (strvalue);

    return (strvalue == NULL ? n : -1);

}

int _pair_decode (char *knots, double **x, double **z)
{
    char *buffer;
    char *iter;
    int N, n;

    double *X, *Z;

    buffer = malloc (strlen (knots) + 1);
    memcpy (buffer, knots, strlen (knots) + 1);


    iter = strtok (buffer, ";");
    N = 1;

    while ((iter = strtok (NULL, ";")) != NULL)
        N++;

    X = malloc (sizeof (double) * N);
    Z = malloc (sizeof (double) * N);

    memcpy (buffer, knots, strlen (knots) + 1);

    n = 0;
    iter = strtok (buffer, ",");

    while (iter != NULL) {
        X[n] = atof (iter);

        iter = strtok (NULL, ";");
        if (iter != NULL)
            Z[n++] = atof (iter);
        else {
            return -1;
        }
        iter = strtok (NULL, ",");
    }

    *x = X;
    *z = Z;

    free (buffer);
    return (n < N ? -1 : N);
}

void surface_shot_consistency (cshotp *p, shot_t *shot, char *shotstr)
{

    if (!((shot->st0 <= shot->st1) && (shot->st1 <= shot->st2) && (shot->st2 <= shot->st3))) {
        fprintf (stderr, "\nERROR: stations in shot specification should be increasing\n");
        exit (EXIT_FAILURE);
    }

    if (!(isinrange (st2x (p, shot->sx), p->xmin, p->xmax) &&
          isinrange (st2x (p, shot->st0), p->xmin, p->xmax) && isinrange (st2x (p, shot->st3), p->xmin, p->xmax))) {
        fprintf (stderr, "\nERROR: problem with shot: %s", shotstr);
        fprintf (stderr, "\nERROR: Experiment out or at border of the model");
        fprintf (stderr, "\nShot at %.4f (%.2f) and receivers between %.4f (%i) and %.4f (%i)\n",
                 st2x (p, shot->sx), shot->sx, st2x (p, (double) shot->st0), shot->st0, st2x (p, (double) shot->st3), shot->st3);

        exit (EXIT_FAILURE);
    }
}

void surface_shot_decode (cshotp *p, int ii)
{
    int ret;

    ret = sscanf (p->shotstr[ii], "%i,%i,%i,%i,%lf,%lf",
                  &(p->shot[ii].st0), &(p->shot[ii].st1), &(p->shot[ii].st2), &(p->shot[ii].st3), &(p->shot[ii].sx), &(p->shot[ii].sz));

    if (ret < 6) {
        fprintf (stderr, "\nERROR: missing entries in shot description\n");
        exit (EXIT_FAILURE);
    }

    surface_shot_consistency (p, &(p->shot[ii]), p->shotstr[ii]);


    p->shot[ii].ntr = (p->shot[ii].st3 - p->shot[ii].st2 + 1) + (p->shot[ii].st1 - p->shot[ii].st0 + 1);
}

int downhole_shot_decode (cshotp *p, int ii)
{
    int ret;

    ret = sscanf (p->shotstr[ii], "%i,%i,%i,%i", &(p->shot[ii].st0), &(p->shot[ii].st1), &(p->shot[ii].st2), &(p->shot[ii].st3));

    p->shot[ii].ntr = (p->shot[ii].st3 - p->shot[ii].st2 + 1) + (p->shot[ii].st1 - p->shot[ii].st0 + 1);

    if (!(isinrange (st2x (p, p->shot[ii].st0), p->xmin, p->xmax) && isinrange (st2x (p, p->shot[ii].st3), p->xmin, p->xmax))) {
        fprintf (stderr, "\nERROR: problem with shot: %s", p->shotstr[ii]);
        fprintf (stderr, "\nERROR: Experiment out of the model");
        fprintf (stderr, "\nReceivers between %f (%i) and %f (%i)\n",
                 st2x (p, p->shot[ii].st0), p->shot[ii].st0, st2x (p, p->shot[ii].st3), p->shot[ii].st3);
    }

    return (ret != 4);
}

void pair_decode (char **knots, double **x, double **z, int *n)
{
    int N = 0;

    while (knots[N] != NULL) {

        /* Split pairs (x,z) into x and z vectors */
        n[N] = _pair_decode (knots[N], x + N, z + N);
        if (n[N] < 0) {
            fprintf (stderr, "Problem in interface %i\n", N);
            exit (EXIT_FAILURE);
        }
        N++;
    }
}

/* Work-around for bug in Cshot */
void work_around (cshotp *p)
{
    int ii;

    p->N++;

    p->x = (double **) realloc (p->x, sizeof (double *) * p->N);
    p->z = (double **) realloc (p->z, sizeof (double *) * p->N);
    p->n = (int *) realloc (p->n, sizeof (int) * p->N);
    p->v = (double *) realloc (p->v, sizeof (double) * p->N);

    for (ii = p->N - 1; ii > 1; ii--) {

        p->x[ii] = p->x[ii - 1];
        p->z[ii] = p->z[ii - 1];
        p->n[ii] = p->n[ii - 1];
        p->v[ii] = p->v[ii - 1];
    }

    p->x[1] = (double *) malloc (sizeof (double) * p->n[2]);
    p->z[1] = (double *) malloc (sizeof (double) * p->n[2]);
    p->v[1] = p->v[0];

    memcpy (p->x[1], p->x[2], sizeof (double) * p->n[2]);
    memcpy (p->z[1], p->z[2], sizeof (double) * p->n[2]);

    for (ii = 0; ii < p->n[1]; ii++)
        p->z[1][ii] *= (0.9);
}

int fill_in_cshotp (cshotp *p, struct parse_params *pp)
{
    int ii;

    /* Figure out how many interfaces are provided */
    p->N = 0;
    while (pp->knots[p->N] != NULL)
        p->N++;

    p->x = (double **) malloc (sizeof (double *) * p->N);
    p->z = (double **) malloc (sizeof (double *) * p->N);
    p->n = (int *) malloc (sizeof (int) * p->N);

    pair_decode (pp->knots, p->x, p->z, p->n);

    /* Compute bounding box of the model */
    p->xmin = p->x[0][0];
    p->xmax = p->x[0][0];
    p->zmin = p->z[0][0];
    p->zmax = p->z[0][0];

    for (ii = 0; ii < p->N; ii++) {
        int k;

        for (k = 0; k < p->n[ii]; k++) {
            p->xmin = min (p->xmin, p->x[ii][k]);
            p->xmax = max (p->xmax, p->x[ii][k]);
            p->zmin = min (p->zmin, p->z[ii][k]);
            p->zmax = max (p->zmax, p->z[ii][k]);
        }
    }

    /* Set bounding box for plot if not provided at command-line */
    p->wxmin = (isnan (p->wxmin) ? p->xmin : p->wxmin);
    p->wzmin = (isnan (p->wzmin) ? p->zmin : p->wzmin);
    p->wxmax = (isnan (p->wxmax) ? p->xmax : p->wxmax);
    p->wzmax = (isnan (p->wzmax) ? p->zmax : p->wzmax);

    /* Velocities */
    p->v = (double *) malloc (sizeof (double) * p->N);
    convert_double (pp->v, p->v, p->N, "v");

#ifdef CSHOT_BUG
    work_around (p);
#endif

    if (p->downhole) {
        int N;

        pair_decode (&(pp->well), &(p->wx), &(p->wz), &(p->wn));

        N = (p->shot_regular ? 1 : p->nshots);

        p->shot = (shot_t *) malloc (sizeof (shot_t) * N);
        for (ii = 0; ii < N; ii++) {
            if (downhole_shot_decode (p, ii))
                exit (EXIT_FAILURE);
        }
    } else {                    /* Surface acquisition */

        if (p->shot_regular) {
            shot_t s;
            char str[40];

            p->shot = (shot_t *) malloc (sizeof (shot_t));
            surface_shot_decode (p, 0);

            s.sz = p->shot[0].sz;

            /* check regular shots */
            for (ii = 1; ii < p->nshots; ii++) {

                s.st0 = p->shot[0].st0 + ii * p->dxshot;
                s.st1 = p->shot[0].st1 + ii * p->dxshot;
                s.st2 = p->shot[0].st2 + ii * p->dxshot;
                s.st3 = p->shot[0].st3 + ii * p->dxshot;
                s.sx = p->shot[0].sx + ii * p->dxshot;

                sprintf (str, "%i,%i,%i,%i,%.2f,%.2f", s.st0, s.st1, s.st2, s.st3, s.sx, s.sz);

                surface_shot_consistency (p, &s, str);
            }

        } else {
            int N = p->nshots;
            p->shot = (shot_t *) malloc (sizeof (shot_t) * N);
            for (ii = 0; ii < N; ii++)
                surface_shot_decode (p, ii);
        }
    }

    return 0;
}
