include Makefile.config

SUBDIRS=src gebr

all:
	for d in $(SUBDIRS); do \
	cd $$d && make && cd -; \
	done

install:
	for d in $(SUBDIRS); do \
	cd $$d; make install; cd -; \
	done

clean:
	for d in $(SUBDIRS); do \
	cd $$d; make clean; cd -; \
	done
