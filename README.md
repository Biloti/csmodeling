# csmodeling -- a Cshot modeling wrapper

This program is a command-line wrapper to ray-tracing modeling tool
Cshot, distributed as part of Seismic Un\*x. By means of csmodeling,
Cshot can be invoked and controlled directly by command-line options,
instead of having its parameters read from several configuration
files. This allows a complete and transparent setup, ideal for batch
execution of Cshot, as well as for the integration of Cshot with the
seismic processing framework of PyGêBR.

Besides, csmodeling generates high quality PDF/SVG figures depicting
the model and/or rays, through Gnuplot. The Gnuplot source script and
data is kept, so that they can be further tuned by the user.

## Build and install

Start by clone this repository with

    # Install GIT in case you don't have it yet
    sudo apt install -y git
    git clone https://gitlab.com/Biloti/csmodeling
    cd csmodeling

csmodeling has very few dependendencies, namely GLib and Gnuplot, as
well as Cshot, as provided by Seismic Un\*x. On Debian and Ubuntu,
these dependecies are installed with

    sudo apt install -y libglib2.0-dev gnuplot

To install Seismic Un\*x, see the instructions in
https://wiki.seismic-unix.org or follow the [tutorial](https://gitlab.com/Biloti/pygebr/-/blob/main/ubuntu-install.md)
provided by PyGêBR.

To build and test csmodeling, use

    make
    cd src && make test

The output for this test is located in /tmp/csmtest.

To install csmodeling, edit the very first line of Makefile.conf to
specify the installation path. After that, just issue the command

    make install

The default path prescribed in `Makefile.confing` is suitable for
system-wide installation and, therefore, root privileges are required.
To install csmodeling in user space, a proper setup for `Makefile.confing`
would be

    PREFIX=~/.local
    BASE_DIR=$(PREFIX)


## Note about Cshot

Cshot, as compiled by default, has a rather small limit to the number
of samples per trace. This limit can be easly increased by editing one
of its source file. In the path `$(CWPROOT)/src/Fortran/Cshot`, edit
file `cshot2.f`. In line 100, increase the value set to `MAXTPT`
(originally defined to 1001). After that, recompile Cshot with `make
fremake`. If you installed SU following the [tutorial](
https://gitlab.com/Biloti/pygebr/-/blob/main/ubuntu-install.md)
provided by PyGêBR, then you are in good shape, as that procedure does
all the adjustments in Cshot code.

Besides, depending on your system setup (Fortran compiler and libraries
version), Cshot may present a bug which prevent computing reflections
on the first interface. A work-around to this bug is implemented and
is active in csmodeling by default. To deactivate it, comment out the
line `WORK_AROUND = -DCSHOT_BUG` in `src/Makfile`.

Copyright 2016-2021 Ricardo Biloti <biloti@unicamp.br>,  
Computational Geophysics Group,  
Applied Math. Department,  
IMECC / UNICAMP.
